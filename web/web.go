package main

import (
	"time"

	"github.com/bep/debounce"
	"github.com/go-humble/locstor"
	"github.com/gopherjs/jquery"
	"gitlab.com/chrissexton/togoist"
)

var jQuery = jquery.NewJQuery

func convert(tmpl, json string, cb func(error, string)) {
	go func() {
		res, err := togoist.TogoString(tmpl, json)
		cb(err, res)
	}()
}

func handleChange() {
	tmpl := jQuery("#tmpl").Val()
	input := jQuery("#input").Val()
	convert(tmpl, input, func(err error, out string) {
		if err != nil {
			jQuery("#output").SetText(err.Error())
			return
		}
		locstor.SetItem("tmpl", tmpl)
		locstor.SetItem("input", input)
		jQuery("#output").SetText(out)
	})
}

func getLocalStorage() {
	if tmpl, err := locstor.GetItem("tmpl"); err == nil {
		jQuery("#tmpl").SetText(tmpl)
	}
	if input, err := locstor.GetItem("input"); err == nil {
		jQuery("#input").SetText(input)
	}
	handleChange()
}

func main() {
	debounced, _ := debounce.New(time.Second)
	jQuery("#tmpl").On(jquery.KEYUP, func() { debounced(handleChange) })
	jQuery("#input").On(jquery.KEYUP, func() { debounced(handleChange) })
	getLocalStorage()
}
