package togoist

import (
	"bytes"
	"encoding/json"
	"html/template"
)

func TogoString(tmpl, jsonInput string) (string, error) {
	t := template.Must(template.New("togoist").Parse(tmpl)) // must is probably wrong
	var j interface{}
	err := json.Unmarshal([]byte(jsonInput), &j)
	if err != nil {
		return "", err
	}
	b := []byte{}
	out := bytes.NewBuffer(b)
	err = t.Execute(out, j)
	if err != nil {
		return "", err
	}
	return out.String(), nil
}
func Togo(tmpl, jsonInput []byte) (string, error) {
	return TogoString(string(tmpl), string(jsonInput))
}
