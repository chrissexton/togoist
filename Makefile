all: web togoist

.PHONY: web

web:
	$(MAKE) -C web

togoist: cmd/togoist/togoist.go
	go build ./cmd/togoist

clean:
	rm -f togoist
	$(MAKE) -C web clean
