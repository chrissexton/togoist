package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/chrissexton/togoist"
)

var tmplPath = flag.String("tmpl", "template.htm", "template file to parse")
var inputPath = flag.String("json", "", "JSON input (empty for stdin)")

func main() {
	flag.Parse()

	f, err := os.Open(*tmplPath)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Could not open template file.\n")
		return
	}
	tmplBytes, err := ioutil.ReadAll(f)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Could not read template file.\n")
		return
	}

	if *inputPath != "" {
		f, err = os.Open(*tmplPath)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Could not open input file.\n")
			return
		}
	} else {
		f = os.Stdin
	}
	inputBytes, err := ioutil.ReadAll(f)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Could not read input file.\n")
		return
	}

	out, err := togoist.Togo(tmplBytes, inputBytes)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Could not parse the template.\n%s\n", err)
		return
	}
	fmt.Print(out)
}
