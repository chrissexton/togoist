# Togoist

Togoist is a small program to facilitate using Go's template library to generate CSV templates for Todoist.

## Installation

* `go get gitlab.com/chrissexton/togoist`
* `make`

## Usage

Open the `index.html` file in the web directory and modify the template and input fields or run `togoist -h`

## License

WTFPL
